
const STATE = {
    OPEN: 'open',
    CLOSED: 'closed'
}

export default class Modal {
    constructor(refElm, options) {
        let defaults = {
            position: 'fixed',
            beforeOpen: function() { console.log('default beforeOpen') },           
            afterOpen: function() {}, 
            beforeClose: function() {}, 
            afterClose: function() {}
        }
        this.refElm = refElm
        this.state = STATE.CLOSED
        this.options = Object.assign({}, defaults, options)
        this.modalWrapper = document.createElement('div')
        this.modalWrapper.onclick = e => e.stopPropagation()
    }

    init() {
        // insert instead of append
        // let pos = this.refElm.getBoundingClientRect()
        // this.modalWrapper.style.top = pos.y + pos.height
        // this.modalWrapper.style.left = pos.x
        // this.refElm.parentNode.insertBefore(this.modalWrapper, this.refElm.nextSibling)
        this.modalWrapper.style.position = this.options.position
        this.state = STATE.OPEN
        this.options.beforeOpen.call(this)        
    }

    toNodes(html) {
        return new DOMParser().parseFromString(html, 'text/html')//.body.childNodes
    }
    
    onCancelButtonClick() {
        console.log('cancel button click');
    }

    toggle() {
        if (this.state === STATE.OPEN) {
            this.state = STATE.CLOSED
            this.modalWrapper.style.visibility = 'hidden'
        } else {
            this.state = STATE.OPEN
            this.modalWrapper.style.visibility = 'visible'
        }
    }

    setContent(content) {
        let dom = this.toNodes(content)
        let cancel = dom.querySelector('#cancel')
        if (cancel) {
            cancel.onclick = this.onCancelButtonClick
        }
        for (let o of dom.body.childNodes) {
            this.modalWrapper.appendChild(o)
        }
        this.refElm.appendChild(this.modalWrapper)
    }

    addClasses(classList = []) {
        classList.forEach(value => {
            if (typeof value === 'string') {
                this.modalWrapper.classList.add(value)
            }
        })
    }

}
