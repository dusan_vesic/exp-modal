
import Modal from './Modal'

let callbacks = {
    fn1() {
        console.log('fn1')
    },
    fn2() {
        console.log('fn2')
    },
    fn3() {
        console.log('fn3')
    },
    fn4() {
        console.log('fn4')
    },
}

function getElements(elmSel) {
    return document.querySelectorAll(elmSel)
}  

function getElement(elmSel) {
    return document.querySelector(elmSel)
}  

function getContentFromService() {
    return Promise.resolve(`
        <div id='form'>
            <h2>Hello, world</h2>
            <div><input /></div>
            <div><input /></div>
            <div><input type='checkbox' /></div>
            <button id='cancel'>cancel</button>
        </div>
    `)
}

// modals holder
let modals = {}
getElements(".modal").forEach((elm) => {
    let id = elm.getAttribute('data-id')
    let beforeOpen = elm.getAttribute('data-before-open')
    let options = {} // options to pass to ctor
    if (beforeOpen) { // if there is fn name passed as attr add it to options obj
        options['beforeOpen'] = callbacks[beforeOpen] 
    }
    elm.onclick = function(evt) {
        if (Object.keys(modals).indexOf(id) < 0) { // add modal if there is modal w/ that id in modals obj
            let modal = new Modal(elm, options)
            modal.init()
            getContentFromService()
                .then(res => {
                    modal.setContent(res)
                    modal.addClasses(['white', 'black'])
                    modals[id] = modal
                })
        } else {
            modals[id].toggle()
        }
    }
})

let standalone = []
getElement('#standalone').onclick = (evt) => {
    let modal;
    if (standalone.length > 0) {
        modal = standalone[0]
        modal.modalWrapper.remove()
        standalone = [] // reset to zero
    } else {
        let options = {}
        let modal = new Modal(document.querySelector('body'), options)
        standalone.push(modal)
        modal.addClasses(['standalone'])
        modal.init()
        modal.setContent(`
            <div>
                <h2>Single modal</h2>
                <h3>Single modal</h3>
            </div>
        `)
        document.querySelector('body').classList.toggle('modalHolder')
    }
}